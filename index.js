const Discord = require("discord.js");
const config = require("./config.json"); //all config settings will reside here.
const client = new Discord.Client();


client.on("ready", () => {
  console.log(`Bot has started, with ${client.users.size} users, in ${client.channels.size} channels of ${client.guilds.size} guilds.`); 
  client.user.setActivity(`Serving ${client.guilds.size} servers`);
  client.user.setActivity(`Use !dayz for available commands`);
});

client.on("guildCreate", guild => {
  console.log(`New guild joined: ${guild.name} (id: ${guild.id}). This guild has ${guild.memberCount} members!`);
  client.user.setActivity(`Serving ${client.guilds.size} servers`);
});

	client.on("guildDelete", guild => {
  console.log(`I have been removed from: ${guild.name} (id: ${guild.id})`);
  client.user.setActivity(`Serving ${client.guilds.size} servers`);
});


client.on("message", async message => {
  if(message.author.bot) return;
  if(message.content.indexOf(config.prefix) !== 0) return;
  const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
  const command = args.shift().toLowerCase();
  
  
  
  if(command === "ping") {
	    const m = await message.channel.send("Ping?");
    m.edit(`Pong! Latency is ${m.createdTimestamp - message.createdTimestamp}ms. API Latency is ${Math.round(client.ping)}ms`);
  }
  


if(command === "dayzplayers") {
var rp = require('request-promise');


var options = {
    contentType: 'application/x-www-form-urlencoded',
    uri: 'https://omegax.cftools.de/api/v1/playerlist/'+ config.serviceid +'',
    method: 'POST',
    resolveWithFullResponse: false,
    headers: {'User-Agent': 'CFTools ServiceAPI-Client'},
    formData: { service_api_key : ''+ config.service_api_key + '' }, 
    json: true 
};

var options2 = {
    contentType: 'application/x-www-form-urlencoded',
    uri: 'https://omegax.cftools.de/api/v1/serverinfo/'+ config.serviceid +'',
    method: 'POST',
    resolveWithFullResponse: false,
    headers: {'User-Agent': 'CFTools ServiceAPI-Client'},
    formData: { service_api_key : ''+ config.service_api_key + '' }, 
    json: true 
};

rp(options2)
    .then(function (body1) {
    console.log(body1.servername)
    console.log(body1.version)
    rp(options)
        .then(function (body) {
        var player_count = JSON.stringify(body.player_count);
        var server_version = JSON.stringify(body1.version);
        var server_name = JSON.stringify(body1.servername);
        console.log(player_count)
        message.channel.send(
            
            'Servername: `'+ server_name +'`\nServerVersion: `'+ server_version +'`\nThere are currently `'+ player_count +'` player(s) online \n'
            )

                for (var i = 0; i < body.players.length; i++) {
                    var value = JSON.stringify(body.players[i].info.name);
                    //console.log(body.players[i].info.name + "=" + value);
                    console.log( value);
                    //var player_names = JSON.stringify(body.players[0].info.name)
                    //message.channel.send(''+ value + '')
                    var test = [];
                    test.push(value);
                    console.log(value);
                    message.channel.send(''+ value + '')
                }
                
        })
    })

    .catch(function (err) {
        message.channel.send('yup.. Houston we have a problem')
    });
}

if(command === "dayzrestart") {
    if(!message.member.roles.some(r=>[''+ config.restart_roles + ''].includes(r.name)) )
      return message.reply("Sorry, you don't have permissions to use this!");
    
       var rp = require('request-promise');
    var options3 = {
        contentType: 'application/x-www-form-urlencoded',
        uri: 'https://omegax.cftools.de/api/v1/invokerestart/'+ config.serviceid +'',
        method: 'POST',
        resolveWithFullResponse: false,
        headers: {'User-Agent': 'CFTools ServiceAPI-Client'},
        formData: { service_api_key : ''+ config.service_api_key + '' }, 
        json: true 
    };

    rp(options3)
    .then(function (body) {
    console.log(body)
    var body2 = JSON.stringify(body);
    message.channel.send(
         'Server is now restarting, will be back online in a minute..'
        )
    }) 

}

if(command === "dayzmessage") {
    if(!message.member.roles.some(r=>[''+ config.restart_roles + ''].includes(r.name)) )
      return message.reply("Sorry, you don't have permissions to use this!");
    
    var message_args = args.join(" ");    
    var rp = require('request-promise');
    var options3 = {
        contentType: 'application/x-www-form-urlencoded',
        uri: 'https://omegax.cftools.de/api/v1/servermessage/'+ config.serviceid +'',
        method: 'POST',
        resolveWithFullResponse: true,
        headers: {'User-Agent': 'CFTools ServiceAPI-Client'},
        formData: { 
            service_api_key : ''+ config.service_api_key + '',
                //params: {
                    message : '' + message_args + ''//,
                //}
            }, 
        json: true 
    };

    rp(options3)    
    .then(function (body) {
    console.log(body.statusCode)
    message.channel.send('Sending message to all players on server: '+ message_args +'');
}) 
}

if(command === "dayzrules") {
    message.channel.send(''+ config.dayzrules +'')
}

if(command === "dayz") {
    message.author.send(''+ message.author +' ```Available commands are\n!dayzrules for server rules\n!dayzplayers for server/player info\n!dayzrestart for a server restart (admin only :))\n!dayzmessage <message> (admin only :))\n!dayzschedule Shows when the server will restart```')
}

if(command === "dayzschedule") {
    message.channel.send(''+ config.dayzschedule +'')
}

});

client.login(config.token);
