# DayZDiscordBot
This nodejs application is Discord Bot for querying CFtools and post it in a discord channel or private message.

Big thanks to Phillip for supplying the DayZ community with the amazing tools: https://cftools.de
This would not have been possible without him!

Uses the following Nodejs modules: 
* https://discord.js.org
* https://github.com/request/request
* https://github.com/request/request-promise

# How to

 - Download and install Nodejs https://nodejs.org/en/download/

 - Create a folder where you want the bot files to run from.

 - Download or pull index.js and config.json from https://gitlab.com/Spud/dayzdiscordbot
 - Change settings in config.json: 

    ```json
    "token"  : "discord bot token here",
    "prefix" : "!",
    "serviceid" : "put your cftools service id here", *
    "service_api_key" : "put your cftools api key here", *
    "restart_roles" : "@Admins", \\set this to your discord admin group
    "dayzrules" : "``` create rules \n for your server \n like this \n multiple lines \n can be seperated with \n```",
    "dayzschedule" : "`Server restarts are scheduled at 20:00 / 00:00 / 04:00 / 08:00 / 12:00 / 16:00 GMT +1 every day`"
    ```
    
 - Place both files in the created folder.
 - Open a command promt in that folder.

 - Execute the following:
 - npm install discord.js
 - npm install request
 - npm install request-promise
 - node index.js

Your bot should now be running and join your Discord server!

\*_Check your service tab on your cftools server dashboard_

Use this guide to set up the prerequisits for nodejs.
https://anidiots.guide/

# Support:
Join here!
[Bot Support Discord Server, feel free to come and test the bot](https://discord.gg/n2y6h3C "Bot Support Discord Server, feel free to come and test the bot")

# Features:
* !dayz gives a list of available bot commands
* !dayzplayers gives you a list of players on the server
* !dayzrestart will restart your dayz server, set restart_roles in config to a group you want to be able to restart the server
* !ping no real use, you can use it to test your bot on your discord server.
* !dayzmessage <message> message will be send to global chat. 
* !dayzschedule displays the restart schedule, set this in config.json. 
* !dayzrules allows you to set server rules in discord.

# Working on:
* perfecting the bot.

# Disclaimer:
* Yes, i am a javascript noob, i made this in a couple of hours to get basic functionality.
* Yes, it works. Yes it can be alot better and Yes! it can and will have more features.
* Yes, you are free to copy this code and use it for your own bot.
